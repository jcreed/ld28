var int = Math.floor;
var rand = Math.random;

var noops = [
  "Have a pint down the way with some friends",
  "Watch the big game",
  "Go to a show",
  "Nap in the park",
  "Have dinner with a cousin",
];

var ignominious_deaths = [
  "You are hit by a bus",
  "You are eaten by a rabid nebu",
  "You lose the will to go on",
  "You die from a fever",
  "You die from pnemonia",
  "You die in a random violent encounter",
  "You die from starvation",
];

var jobs = [
  {name: "Editor", available: 0.8,
   tasks: [
     {text: "Contribute to a volume of poetry", base: 5, skill_use: ["Music", "Reading"]},
     {text: "Organize journalism conference", base: 10, skill_use: ["Organizations", "Logistics", "Reading"]},
     {text: "Get the scoop on a political scandal", base: 10, skill_use: ["Persuasion", "Writing"]},
     {text: "Light magazine article", base: 1, skill_use: ["Business", "Writing"]},
     {text: "Unabridged dictionary", base: 10, skill_use: ["Reading", "Writing"]},
     {text: "Political science textbook", base: 10, skill_use: ["Reading", "Communication"]},
     {text: "Medical pamphlet", base: 1, skill_use: ["Reading", "Medicine"]},
     {text: "Athletic catalog", base: 10, skill_use: ["Reading", "Arrowsport", "Ballsport"]},
     {text: "Underground zine", base: 2, skill_use: ["Writing", "Subversion"]},
     {text: "Legal journal", base: 20, skill_use: ["Reading", "Writing", "Law"]},
     {text: "Encyclopedia article", base: 2, skill_use: ["Reading", "Writing", "Chemistry"]},
   ]},
  {name: "Senator", available: 0.9,
   tasks: [
     {text: "Kiss babies", base: 1, skill_use: ["Devotion", "Acting"]},
     {text: "Talk with the common people", base: 1, skill_use: ["Communication"]},
     {text: "Understand dire plight", base: 1, skill_use: ["Empathy"]},
     {text: "Go door to door", base: 1, skill_use: ["Driving"]},
     {text: "Exploit loophole", base: 5, skill_use: ["Law"]},
     {text: "Have drinks with local leaders", base: 5, skill_use: ["Business"]},
     {text: "Make pragmatic compromise", base: 5, skill_use: ["Logistics", "Peace"]},
     {text: "Raise money", base: 1, skill_use: ["Finance"]},
     {text: "Win local election", min: 10, base: 10, skill_use: ["Persuasion"]},
     {text: "Populist hero", min: 10, base: 10, skill_use: ["Subversion"]},
     {text: "Win district election", min: 100, base: 100, skill_use: ["Persuasion"]},
     {text: "Win senate election", min: 500, base: 500, skill_use: ["Persuasion", "Organizations"]},
   ]},
  {name: "Lawyer", available: 0.8,
   tasks: [
     {text: "Win criminal case", base: 50, skill_use: ["Law", "Persuasion"]},
     {text: "Win corporate lawsuit", base: 50, skill_use: ["Law", "Business"]},
     {text: "Exploit loophole", base: 50, skill_use: ["Law", "Subversion"]},
     {text: "Promotion at the firm", min: 500, base: 50, skill_use: ["Law", "Organizations"]},
   ]},
  {name: "Builder", available: 0.9,
   tasks: [
     {text: "Condominiums", base: 50, skill_use: ["Carpentry", "Business"]},
     {text: "Office building", base: 50, skill_use: ["Carpentry", "Finance", "Architecture"]},
     {text: "Monument", base: 50, skill_use: ["Carpentry", "Peace"]},
     {text: "Stollarong Residences", base: 50, skill_use: ["Carpentry", "Languages"]},
     {text: "Parking lot", base: 50, skill_use: ["Architecture", "Driving"]},
     {text: "Hospital", base: 50, skill_use: ["Architecture"]},
     {text: "Absurdist apartments", base: 50, skill_use: ["Architecture", "Subversion"]},
   ]},
  {name: "Singer", available: 0.7,
   tasks: [
     {text: "Moving ballad", base: 10, skill_use: ["Music", "Peace"]},
     {text: "Angry teen angst hit", base: 10, skill_use: ["Music", "Empathy"]},
     {text: "All-coast tour", base: 10, skill_use: ["Music", "Driving"]},
     {text: "Rad album cover", base: 10, skill_use: ["Drawing"]},
     {text: "Radio gold", min: 100, base: 100, skill_use: ["Music", "Communication"]},
     {text: "Viral video", min: 10, base: 900, skill_use: ["Music", "Subversion", "Acting"]},
   ]},
  {name: "Monastic", available: 0.9,
   tasks: [
     {text: "Contemplate all", base: 50, skill_use: ["Meditation"]},
     {text: "Care for the needy", base: 50, skill_use: ["Empathy", "Communication"]},
     {text: "Collect donations", base: 50, skill_use: ["Driving"]},
     {text: "Translate ancient texts", base: 50, skill_use: ["Languages"]},
   ]},
  {name: "Theorist", available: 0.7,
   tasks: [
     {text: "A unified account of chemical reactions", base: 50, skill_use: ["Chemistry", "Math"]},
     {text: "Principles of economics", base: 50, skill_use: ["Finance", "Business", "Math"]},
     {text: "Fundamentals of storytelling", base: 50, skill_use: ["Communication", "Languages", "Reading"]},
     {text: "Why we play", base: 50, skill_use: ["Ballsport", "Arrowsport"]},
     {text: "Aesthetics explained", base: 50, skill_use: ["Drawing", "Peace"]},
   ]},
  {name: "Trucker", available: 0.9,
   tasks: [
     {text: "Make a run", base: 50, skill_use: ["Driving"]},
     {text: "Go all night", base: 50, skill_use: ["Devotion"]},
     {text: "Union president", min: 500, base: 50, skill_use: ["Persuasion", "Organizations"]},
   ]},
  {name: "Athlete", available: 0.5,
   tasks: [
     {text: "Ballsport victory", base: 50, skill_use: ["Ballsport"]},
     {text: "Arrowsport victory", base: 50, skill_use: ["Arrowsport"]},
     {text: "Endorsement deal", base: 50, skill_use: ["Ballsport", "Business"]},
     {text: "Inspirational autobiography", min: 500, base: 50, skill_use: ["Arrowsport", "Writing"]},
     {text: "Ghostwritten autobiography", min: 500, base: 1, skill_use: ["Arrowsport"]},
     {text: "Win double championship", min: 500, base: 500, skill_use: ["Arrowsport", "Ballsport"]},
   ]},
  {name: "Executive", available: 0.5,
   tasks: [
     {text: "Form committee", base: 50, skill_use: ["Oranizations"]},
     {text: "Read memo", base: 50, skill_use: ["Reading"]},
     {text: "Write memo", base: 50, skill_use: ["Writing"]},
     {text: "Make decision", base: 50, skill_use: ["Devotion"]},
   ]},
  {name: "Mendicant", available: 1.0,
   tasks: [
     {text: "Beg", base: 1, skill_use: ["Meditation"]},
     {text: "Attain understanding", base: 1, skill_use: ["Meditation", "Peace", "Empathy", "Devotion", "Communication"]},
   ]},
  {name: "Pharmacist", available: 0.5,
   tasks: [
     {text: "Process prescriptions", base: 50, skill_use: ["Reading"]},
     {text: "Adjust dosage", base: 50, skill_use: ["Chemistry"]},
   ]},
  {name: "Mediator", available: 0.8,
   tasks: [
     {text: "Solve interpersonal conflict", base: 50, skill_use: ["Empathy", "Peace"]},
     {text: "Solve corporate conflict", base: 50, skill_use: ["Communication", "Business"]},
   ]},
  {name: "Calligrapher", available: 0.7,
   tasks: [
     {text: "New font", base: 50, skill_use: ["Drawing", "Subversion"]},
     {text: "Signwriting", base: 50, skill_use: ["Drawing"]},
     {text: "Infographics", base: 50, skill_use: ["Drawing", "Communication"]},
   ]},
  {name: "Seller", available: 0.9,
   tasks: [
     {text: "Move some vacuum cleaners", base: 50, skill_use: ["Persuasion"]},
     {text: "Push some combs", base: 50, skill_use: ["Persuasion"]},
   ]},
  {name: "Chef", available: 0.9,
   tasks: [
     {text: "Work in the kitchen", base: 50, skill_use: ["Cooking"]},
     {text: "Write cookbook", base: 50, skill_use: ["Cooking", "Writing"]},
   ]},
  {name: "Translator", available: 0.9,
   tasks: [
     {text: "Translate novel", base: 50, skill_use: ["Languages", "Writing", "Reading"]},
     {text: "Translate documents", base: 50, skill_use: ["Languages", "Writing", "Reading"]},
   ]},
  {name: "Teacher", available: 0.9,
   tasks: [
     {text: "Math teacher", base: 50, skill_use: ["Communication", "Math"]},
     {text: "Biology teacher", base: 50, skill_use: ["Communication", "Biology"]},
     {text: "Chemistry teacher", base: 50, skill_use: ["Communication", "Chemistry"]},
     {text: "Engineering teacher", base: 50, skill_use: ["Communication", "Engineering"]},
   ]},
  {name: "Doctor", available: 0.8,
   tasks: [
     {text: "Family practice", base: 50, skill_use: ["Medicine"]},
     {text: "Bedside manner", base: 50, skill_use: ["Medicine", "Empathy"]},
     {text: "Health service stragegy", base: 50, skill_use: ["Medicine", "Business"]},
     {text: "Dietary advice", base: 50, skill_use: ["Medicine", "Cooking"]},
     {text: "New ideas for therapy", base: 50, skill_use: ["Biology", "Medicine"]},
     {text: "New ideas for medicine", base: 50, skill_use: ["Chemistry", "Medicine"]},
   ]},
  {name: "Farmer", available: 0.9,
   tasks: [
     {text: "Develop new breed of nebu", base: 50, skill_use: ["Biology"]},
     {text: "Spray for locusts", base: 50, skill_use: ["Logistics"]},
     {text: "Early morning harvest", base: 50, skill_use: ["Devotion"]},
     {text: "Planting", base: 50, skill_use: ["Devotion"]},
     {text: "Got the fox been eating the chickens", base: 50, skill_use: ["Arrowsport"]},
     {text: "Run the combine across the fields", base: 50, skill_use: ["Driving"]},
     {text: "Take crops down market", base: 50, skill_use: ["Business"]},
     {text: "New fertilizer", base: 50, skill_use: ["Chemistry"]},
   ]},
  {name: "Author", available: 0.8,
   tasks: [
     {text: "Realistic novel", base: 50, skill_use: ["Writing", "Reading"]},
     {text: "Speculative novel", base: 50, skill_use: ["Writing", "Reading"]},
     {text: "Historical fiction", base: 50, skill_use: ["Writing", "Reading"]},
     {text: "Short stories", base: 50, skill_use: ["Writing", "Reading"]},
   ]},
  {name: "Contrarian", available: 0.8,
   tasks: [
     {text: "Win appeal", base: 50, skill_use: ["Subversion", "Law"]},
     {text: "Scathing review", base: 50, skill_use: ["Subversion", "Reading"]},
     {text: "Philosophical gadfly", base: 50, skill_use: ["Subversion", "Devotion"]},
   ]},
  {name: "Historian", available: 0.8,
   tasks: [
     {text: "Paper on ancient Birnallongers", base: 50, skill_use: ["Reading", "Writing"]},
     {text: "Paper on first Stollarong War", base: 50, skill_use: ["Reading", "Writing"]},
     {text: "Paper on second Stollarong War", base: 50, skill_use: ["Reading", "Writing"]},
     {text: "Settle academic dispute", base: 50, skill_use: ["Peace"]},
     {text: "Obscure reference", base: 100, skill_use: ["Languages", "Reading"]},
     {text: "Paper on Birnallong sociology", base: 50, skill_use: ["Organizations", "Writing"]},
     {text: "Conference chair", min: 500, base: 100, skill_use: ["Communication", "Reading"]},
   ]},
  {name: "Astronomer", available: 0.8,
   tasks: [
     {text: "Paper on exoplanet composition", base: 50, skill_use: ["Math", "Chemistry"]},
     {text: "Paper on star life-cycle", base: 50, skill_use: ["Math", "Physics"]},
     {text: "Space mission planning", base: 50, skill_use: ["Math", "Biology", "Engineering"]},
     {text: "Conference chair", min: 500, base: 100, skill_use: ["Communication", "Reading"]},
   ]},
  {name: "Inventor", available: 0.55,
   tasks: [
     {text: "Amazing new widget", base: 50, skill_use: ["Engineering"]},
     {text: "Astounding novelty sport", base: 50, skill_use: ["Ballsport", "Subversion"]},
     {text: "Disruptive entreprenurial idea", base: 50, skill_use: ["Business", "Law", "Subversion"]},
   ]},
  {name: "Accountant", available: 0.8,
   tasks: [
     {text: "Tax preparation", base: 50, skill_use: ["Law"]},
     {text: "Personal finances", base: 50, skill_use: ["Law", "Empathy"]},
   ]},
  {name: "Consultant", available: 0.9,
   tasks: [
     {text: "Acquisition committee", base: 50, skill_use: ["Business"]},
     {text: "Portfolio manager", base: 50, skill_use: ["Business", "Finance"]},
     {text: "Hostile takeover", base: 50, skill_use: ["Business", "Persuasion"]},
     {text: "Supply-chain reorg", base: 50, skill_use: ["Business", "Logistics"]},
     {text: "Mission statement", base: 50, skill_use: ["Business", "Meditation"]},
     {text: "Product development", base: 50, skill_use: ["Business", "Engineering"]},
     {text: "Crunch time", base: 50, skill_use: ["Business", "Devotion"]},
     {text: "Patent wrangling", base: 50, skill_use: ["Business", "Law"]},
     {text: "Logo redesign", base: 50, skill_use: ["Business", "Drawing"]},
     {text: "Teambuilding", base: 50, skill_use: ["Business", "Ballsport"]},
   ]},
  {name: "Cabinetter", available: 0.9,
   tasks: [
     {text: "Shelves", base: 50, skill_use: ["Carpentry"]},
     {text: "Bed frame", base: 50, skill_use: ["Carpentry"]},
     {text: "Decorative facade", base: 50, skill_use: ["Carpentry", "Architecture"]},
     {text: "Interactive contraption", base: 50, skill_use: ["Carpentry", "Physics"]},
     {text: "Lovely carven inlay", base: 50, skill_use: ["Carpentry", "Drawing"]},
     {text: "Canoe", base: 50, skill_use: ["Carpentry"]},
     {text: "Ballsport mallet", base: 50, skill_use: ["Carpentry", "Ballsport"]},
     {text: "Arrowsport bow", base: 50, skill_use: ["Carpentry", "Arrowsport"]},
   ]},
  {name: "Comedian", available: 0.8,
   tasks: [
     {text: "Zinger", base: 50, skill_use: ["Acting"]},
     {text: "Improv", base: 50, skill_use: ["Meditation"]},
     {text: "Heartwarming story", base: 50, skill_use: ["Acting", "Peace"]},
     {text: "Self-promotion", base: 50, skill_use: ["Business"]},
     {text: "Goofy cartoons", base: 50, skill_use: ["Acting", "Drawing"]},
     {text: "Social commentary", base: 50, skill_use: ["Acting", "Persuasion"]},
     {text: "Jokes for nerds", base: 50, skill_use: ["Acting", "Engineering"]},
     {text: "Office comedy", base: 50, skill_use: ["Acting", "Organizations"]},
     {text: "Know what I mean?", base: 50, skill_use: ["Acting", "Empathy"]},
   ]},
  {name: "Approver", available: 1.0,
   tasks: [
     {text: "Good book", base: 1, skill_use: ["Reading"]},
     {text: "Bad book", base: 1, skill_use: ["Reading"]},
     {text: "Good movie", base: 1, skill_use: ["Communication", "Acting", "Reading"]},
     {text: "Bad movie", base: 1, skill_use: ["Communication", "Acting", "Reading"]},
     {text: "Good song", base: 1, skill_use: ["Music"]},
     {text: "Bad song", base: 1, skill_use: ["Music"]},
     {text: "Good argument", base: 1, skill_use: ["Persuasion", "Law"]},
     {text: "Bad argument", base: 1, skill_use: ["Persuasion", "Law"]},
     {text: "Brilliant gesamtkunstwerk", base: 1, skill_use: ["Drawing", "Writing", "Acting", "Music", "Languages", "Architecture"]},
     {text: "Wretched gesamtkunstwerk", base: 1, skill_use: ["Drawing", "Writing", "Acting", "Music", "Languages", "Architecture"]},

   ]},
];

var skills = [
  "Reading",
  "Writing",
  "Math",
  "Medicine",
  "Law",
  "Drawing",
  "Carpentry",
  "Business",
  "Driving",
  "Empathy",
  "Persuasion",
  "Cooking",
  "Ballsport",
  "Arrowsport",
  "Meditation",
  "Engineering",
  "Biology",
  "Physics",
  "Architecture",
  "Languages",
  "Devotion",
  "Finance",
  "Logistics",
  "Peace",
  "Chemistry",
  "Communication",
  "Organizations",
  "Subversion",
  "Music",
  "Acting",
];

var NUM_SKILLS = skills.length;
var NUM_JOBS = jobs.length;


skills_inv = {};
for(var i = 0; i < NUM_SKILLS; i++) {
  skills_inv[skills[i]] = i;
}


function roman (num) {
  return ["0", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"][num];
}

function render() {
  $("#month").text(month % 10);
  $("#year").text(int(month / 10));
  for (var i = 0; i < NUM_SKILLS; i++) {
    if (skillsHave[i] == 0) 
      skillsTd[i].css({opacity: 0.2});
    else if (skillsHave[i] == 1) 
      skillsTd[i].css({opacity: 0.7});
    else
      skillsTd[i].css({opacity: 1});
    skillsTd[i].html("<b>" + skills[i] + "</b> " + roman(skillsHave[i]));

    jobsTd[i].css({visibility: "hidden"});
    
    if (mode == "transition") {
      if (jobs[i].active) { 
	jobsTd[i].css({visibility: "visible"});
      }
      jobsTd[i].html("<b>" + jobs[i].name + "</b>");
    }
    else if (mode == "working") {
      $("#status").html("Retirement score: <b>\u042f" + commas(score) + "</b>");
      $("#bio_job").html("<b>" + jobs[curJob].name + "</b>");
      var div = $("#bio_events_div");
      div.scrollTop(div[0].scrollHeight);
    }
  }
}

function commas(x) {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1,$2");
    return x;
}

function end_timer() {
  if (timer != null) {
    clearInterval(timer);
    timer = null;
  }
}

function end_game() {
  render();
  end_timer();
  $("#restartBtn").show();
  mode = "done";
}

function init_game() {
  $("#intro").hide();
  $("#game").show();
  end_timer();
  can_retire = 0;
  mode = "pre";
  month = 0;
  jobsTd = [];
  skillsTd = [];
  skillsHave = [];
  for(var i = 0; i < NUM_SKILLS; i++) {
    skillsHave[i] = 0;
  }

  $("#mainBtn").show();
  $("#status").hide();
  $("#skills").empty();
  var skillsTab = $("<table width=100% height=100%>").appendTo("#skills");
  skillsTab.css({height: "100%"});
  for (var i = 0; i < 10; i++) {
    var tr = $("<tr>").appendTo(skillsTab);
    for (var j = 0; j < 3; j++) {
      var td = $("<td height=10% width=33.3%>").appendTo(tr);
      td.append(skills[j * 10 + i]);
      td.addClass("skillBub");
      skillsTd[j * 10 + i] = td;
    }
  }

  $("#jobs").show();
  $("#jobs").empty();
  var jobsTab = $("<table width=100% height=100%>").appendTo("#jobs");
  jobsTab.css({height: "100%"});
  for (var i = 0; i < 10; i++) {
    var tr = $("<tr>").appendTo(jobsTab);
    for (var j = 0; j < 3; j++) {
      var td = $("<td height=10% width=33.3%>").appendTo(tr);
      td.append(jobs[j * 10 + i]);
      td.addClass("jobBub");
      td.addClass("button");
      td.attr('jobId', j * 10 + i);
      jobsTd[j * 10 + i] = td;
    }
  }
  $("#mainText").text("begin school");
  $("#mainSuppl").text("click and hold");
  $("#bio").hide();
  $("#bio_events_div").empty();
  render();
}

function randi(n) {
  return int(rand() * n);
}

function randelm(ls) {
  return ls[randi(ls.length)];
}

$("#beginBtn").click(init_game);

$("#restartBtn").click(init_game);

$("#jobs").on('mousedown', '.jobBub', function() {
  curJob = $(this).attr('jobId');
  mode = "working";
  show_bio();
});

function show_bio() {
  score = 0;
  $("#bio").show();
  $("#restartBtn").hide();
  $("#jobs").hide();
  $("#bio_events_div").height($("#bio_events").height() - 100);
  render();
}

$(document).mousedown(function(ev) {
  ev.preventDefault();
});


$("#mainBtn").mousedown(function(ev) {
  $("#mainText").text("in school");
  $("#mainSuppl").text("release to graduate");
  month = 120;
  skillsHave[0] = 1;
  skillsHave[1] = 1;
  mode = "school";
  render();
  timer = setInterval(tick, 200);
});

$(document).mouseup(function(ev) {
  if (mode == "school") {
    $("#mainBtn").hide();
    $("#status").html("choose job<br>( click and hold )").show();
    for (var i = 0; i < NUM_SKILLS; i++) {
      jobs[i].active = rand() < jobs[i].available;
    }
    mode = "transition";
    render();
  }
  else if (mode == "working") {
    var div = $("#bio_events_div");
    if (can_retire) {
      div.append('<b>You retire on a comfortable state pension at the age of ' + int(month/10) + '</b><br>');
    } else {
      div.append('<b>' + randelm(ignominious_deaths) + ' at the age of ' + int(month/10) + '</b><br>');
    }
    render();
    end_game();
  }
  
});

function tick() {
  month++;

  if (mode == "school") {
    if (randi(2) == 0) {
      var r = randi(NUM_SKILLS);
      if (skillsHave[r] < 10) {
	skillsHave[r]++;
      }
    }

    if (month >= 700) {
      show_bio();
      var div = $("#bio_events_div");
      $("#bio_job").html("<b>Dilettante</b>");
      div.append('<b>At age 70 you retire, a happy life-long student</b><br>');
      end_game();
    }  
  }
  else if (mode == "transition") {
    if (month >= 700) {
      show_bio();
      $("#status").text("");
      $("#bio_job").html("<b>Derelict</b>");
      var div = $("#bio_events_div");
      div.append('<b>At age 70 you retire shamefully from a life of fruitless indecision</b><br>');
      end_game();
    }  
  }
  else if (mode == "working") {
    var div = $("#bio_events_div");
    if (randi(8) == 0) {
      var q = randelm(jobs[curJob].tasks);
      var act_score = q.base;
      var multipliers = "";
      q.skill_use.forEach(function(r) {
	act_score *= skillsHave[skills_inv[r]];
	multipliers += " * " + r;
      });
      var min = q.min || 0;
      if (act_score > 0 && score >= min) {
	div.append(q.text + "<br><span class='comp'>" + q.base + multipliers + " = " + act_score + "</span><br>");
	score += act_score;
      }
    }
    else if (randi(32) == 0) {
      div.append(randelm(noops) + "<br>");
    }

    if (score >= 1000 && can_retire == 0) {
      div.append('<b>At \u042f1,000 you qualify for early retirement</b><br>');
      can_retire = 1;
    }

    if (month >= 700) {
      div.append('<b>At age 70 you qualify for mandatory retirement</b><br>');
      end_game();
    }  
  }
  render();
}

timer = null;
$("#game").hide();



